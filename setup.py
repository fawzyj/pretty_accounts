from distutils.core import setup
from setuptools import find_packages

setup(
    name='pretty_accounts',
    install_requires=['distribute'],
    version='0.1',
    packages=find_packages(),
    license='',
    long_description=open('README.txt').read(),
    url='http://www.example.com/',
    author='Your Name',
    author_email='yourname@example.com',
    include_package_data=True,
)